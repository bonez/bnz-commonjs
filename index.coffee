through = require 'through2'

capitalizeFirstLetter = (string) -> string.charAt(0).toUpperCase() + string.slice 1

module.exports =

    wrapModule: ->

        stream = through.obj (file, enc, callback) ->

            if file.isNull()
                @push file
                return callback()

            if file.isStream()
                @emit 'error', 'ERROR'
                return callback()

            try
                parts = file.path.split '/'
                name = parts[parts.length - 1].split('.')[0]
                content = file.contents.toString()
                compiled = content

                returnObject  = /#RETURN-OBJECT#/igm.test content
                returnNothing = /#RETURN-VOID#/igm.test content

                if name not in ['init']

                    if returnObject

                        content = content
                        .replace "'use strict'", ''
                        .replace /\n/g, "\n    "
                        .replace "#RETURN-OBJECT#", "module.exports ="
                        compiled = "define '#{name}', (module, require) ->\n    'use strict'\n    #{content}\n    return"

                    else if returnNothing

                        content = content
                        .replace /\n/g, "\n    "
                        .replace "'use strict'", ''
                        compiled = "define '#{name}', ->\n    'use strict'\n\n    #{content}"

                    else

                        content = content
                        .replace "\n    'use strict'", ''
                        .replace "class ", "\nmodule.exports = class "
                        .replace /\n/g, "\n    "
                        compiled = "define '#{name}', (module, require) ->\n    'use strict'\n    #{content}\n    return"

                file.contents = new Buffer compiled
                @push file

            catch err
                @emit 'error', 'try ERROR'

            callback()

        stream


    wrapTemplate: ->

        stream = through.obj (file, enc, callback) ->

            if file.isNull()
                @push file
                return callback()

            if file.isStream()
                @emit 'error', 'ERROR'
                return callback()

            try
                parts = file.path.split '/'
                name = capitalizeFirstLetter parts[parts.length - 1].split('.')[0]
                compiled = file.contents.toString().replace "function template(locals) {", "define('#{name}', function (module, require) {\nmodule.exports = function (locals) {"
                compiled += "\n});"
                compiled = compiled.replace 'buf.push("<!DOCTYPE html>");', ''
                file.contents = new Buffer compiled
                @push file
            catch err
                @emit 'error', 'ERROR'

            callback()

        stream

    jsReplacement: ->

        stream = through.obj (file, enc, callback) ->

            if file.isNull()
                @push file
                return callback()

            if file.isStream()
                @emit 'error', 'ERROR'
                return callback()

            try

                compiled = file.contents
                    .toString()
                    .replace /\s\snew\s\(module.exports\s=\s/igm, '  module.exports = '
                    .replace /\s\s\s\sreturn (.*);\n\n\s\s\}\)\((.*)\)\);/igm, (substr, flag1, flag2) ->
                        if /^[A-Z]/.test(flag1) and /^[A-Z]/.test flag2
                            return "    return new #{flag1};\n\n  \}\)\(#{flag2}\);"
                        substr

                file.contents = new Buffer compiled
                @push file
            catch err
                @emit 'error', 'ERROR'

            callback()


        stream
